const router = require('express').Router()
const restrict = require('./middlewares/restrict')

// controller
const auth = require('./controllers/authController')

// Homepage
// router.get('/', (req, res)=> res.render('index'))

// Register Page
// router.get('/register', (req, res) => res.render('register'))
router.post('/api/v1/auth/register', auth.register)

// Login Page
router.post('/api/v1/auth/login', auth.login)
// whoami
router.get('/api/v1/auth/whoami', restrict, auth.whoami)


module.exports = router;