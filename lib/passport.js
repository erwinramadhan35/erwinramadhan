const passport = require('passport')
const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt')
const { User } = require('../models')

// Passport JWT options
const options = {
    // Untuk mengektract JWT dari request dan mengambil tokenya dari header yang bernama Authorization
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    // Harus sama seperti dengan apa yang kita masukan sebagai parameter kedua dari jwt.sign di User Model.
    // Inilah yang kita pakai untuk memverifikasi apakah tokennyna dibuat oleh sistem kita
    secretOrKey: 'Ini rahasia ga boleh disebar-sebar',
}

passport.use(new JwtStrategy(options, async (payload, done) =>{
    User.findByPk(payload.id)
    .then(user => done(null, user))
    .catch(err => done (err, false))
}))

module.exports = passport