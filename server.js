const express = require('express')
const app = express()
const session = require('express-session')
const flash = require('express-flash')
require('dotenv').config()

// Menseting req body parser, dan harus ditaruh atas bro
app.use(express.urlencoded({ extended: true }))

// Setting session handler
app.use(session({
    secret: 'Buat ini menjadi rahasia',
    resave: false,
    saveUninitialized: false
}))

// setting passport
const passport = require('./lib/passport')
app.use(passport.initialize())

// setting flash

// setting view engine

// setting router
const router = require('./router')
app.use(router)

const port = process.env.PORT || 3000
app.listen(port, () => console.log(`Server Nyala Bos di port ${port}`))